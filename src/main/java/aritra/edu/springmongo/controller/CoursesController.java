package aritra.edu.springmongo.controller;

import java.util.List;
import java.util.Optional;

import aritra.edu.springmongo.document.Course;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import aritra.edu.springmongo.repo.CoursesRepository;

@RestController
@RequestMapping("/courses")
public class CoursesController {

	Logger logger = LoggerFactory.getLogger(CoursesController.class);
	
	@Autowired
	private CoursesRepository repository;


	/**
	 *
	 * @return list of course documents
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Course>> getCourses(){
		
		logger.info("in courses controller /");
		
		List<Course> courses = repository.findAll();
		return new ResponseEntity<>(courses,HttpStatus.OK);
		
	}

	/**
	 *
	 * @param id
	 * @return course document based on id
	 */
	@RequestMapping(method = RequestMethod.GET, value="/{id}")
	public ResponseEntity<Course> getcourseById(@PathVariable String id){
		
		logger.info("in courses controller /{id}");
		
		Optional<Course> course = repository.findById(id);
		if(course.isPresent()) {
			return new ResponseEntity<>(course.get(),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}		
		
	}
	
	
}
