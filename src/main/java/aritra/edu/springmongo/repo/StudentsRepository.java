package aritra.edu.springmongo.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import aritra.edu.springmongo.document.Student;

public interface StudentsRepository  extends MongoRepository<Student, String> {
    Optional<Student> findById(String id);
    List<Student> findAll();
}

